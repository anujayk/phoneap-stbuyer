angular.module('dashboard',[])
	.controller('dashboardCtrl',['$scope',function($scope){


 $scope.groups = [];
  for (var i=0; i<10; i++) {
    $scope.groups[i] = {
      name: i,
      items: []
    };
    for (var j=0; j<3; j++) {
      $scope.groups[i].items.push(i + '-' + j);
    }
  }

 
	$scope.toggleGroup = function(group) {
		
		if($scope.active_class==group){
	    	$scope.active_class=null;
	    	
	    }else{
	    	$scope.active_class=group;
	    }
	};
	
	$scope.isGroupShown = function(group) {
	    return $scope.shownGroup === group;
	};
		
}]);