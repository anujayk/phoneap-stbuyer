angular.module('ngAuthenticate',[])
.controller('authenticateCtrl',['$scope','$ionicLoading','$state',function($scope,$ionicLoading,$state){

		$scope.authenticate=function(param){
			 
			 if(param=='native-login'){
			 	$state.go('signup');
			 }
			 else if(param=='native-signup'){
			 	$state.go('home.nativelogin');
			 }
			 else if(param=='facebook-login'){
			 		$ionicLoading.show({
			 			template:'Connecting To Facebook...'
			 		});
				 setTimeout(function() {
				 	$ionicLoading.hide();
				 	$state.go('dashboard');
				 }, 4000);
				console.log(param);
			 }
			 else if(param='do-native-login'){
			 	$state.go('dashboard');
			 }
			 
		}

}]);